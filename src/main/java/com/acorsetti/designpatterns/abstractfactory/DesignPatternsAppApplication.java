package com.acorsetti.designpatterns.abstractfactory;

import com.acorsetti.designpatterns.abstractfactory.factory.AbstractFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;


@Component
@SpringBootApplication
public class DesignPatternsAppApplication {

    @Value("${concreteFactory}")
    private String concreteFactoryClassName;

    public static void main(String[] args) {
        SpringApplication.run(DesignPatternsAppApplication.class, args);
    }

    @Bean
    public AbstractFactory getConcreteFactory() throws Exception{

        AbstractFactory factory;
        try {
            factory = (AbstractFactory) Class.forName(concreteFactoryClassName).newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
            throw e;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            throw e;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            throw e;
        }
        return factory;

    }
}


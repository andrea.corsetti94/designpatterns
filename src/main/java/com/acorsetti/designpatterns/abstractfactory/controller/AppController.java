package com.acorsetti.designpatterns.abstractfactory.controller;

import com.acorsetti.designpatterns.abstractfactory.factory.AbstractFactory;
import com.acorsetti.designpatterns.abstractfactory.factory.AbstractUIContainer;
import com.acorsetti.designpatterns.abstractfactory.factory.AbstractUIItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class AppController {

    private AbstractFactory abstractFactory;

    @Autowired
    public AppController(AbstractFactory abstractFactory){
        this.abstractFactory = abstractFactory;
    }

    @GetMapping
    public String getDefault(){
        return "{\"message\": \"Hello World\"}";
    }

    @PostMapping("createUI/item/{criteria}")
    public AbstractUIItem useItem(@PathVariable String criteria){

        return this.abstractFactory.createUIItem(criteria);

    }

    @PostMapping("createUI/container/{criteria}")
    public AbstractUIContainer useContainer(@PathVariable String criteria){
        return this.abstractFactory.createUIContainer(criteria);
    }


}

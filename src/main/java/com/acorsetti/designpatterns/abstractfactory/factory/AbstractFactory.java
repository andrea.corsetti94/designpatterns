package com.acorsetti.designpatterns.abstractfactory.factory;


/**
 * La differenza tra l'AbstractFactory e il Factory Method sta nel contesto.
 * Ossia, l'AC è un'interfaccia che si occupa di definire il contratto per istanziare
 * più oggetti relativa a una particolare famiglia (dove per famiglia NON si intende
 * che questi oggetti debbano avere una qualche relazione gerarchica).
 *
 * Il factory method è un metodo di una classe che serve a generare un solo tipo
 * di oggetto. Questo metodo può essere sovrascritto da una classe figlia per istanziare
 * un oggetto di un diverso tipo rispetto a quello istanziato nella superclasse.
 *
 * L'AC è implementata tramite composizione, ossia essa è un attributo della classe client
 * che chiama i metodi esposti dall'interfaccia e viene iniettato tramite DI.
 *
 * Il FM è implementato tramite ereditarietà. Non è altro che un Template Pattern il cui
 * scopo non è orientato al "behaviour" ma alla creazione di oggetti.
 */
public interface AbstractFactory {

    AbstractUIItem createUIItem(String criteria);
    AbstractUIContainer createUIContainer(String criteria);

}

package com.acorsetti.designpatterns.abstractfactory.factory;

public class PopupItem extends AbstractUIItem {

    private String specialName = "PIN";

    PopupItem(String parentClass) {
        super(parentClass);
    }

    @Override
    String name() {
        return super.getParentClass() + " PopupItemName: " + specialName;
    }

    public String getSpecialName() {
        return specialName;
    }

    public void setSpecialName(String specialName) {
        this.specialName = specialName;
    }
}

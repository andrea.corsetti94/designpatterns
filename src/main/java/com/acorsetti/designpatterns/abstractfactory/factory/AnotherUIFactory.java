package com.acorsetti.designpatterns.abstractfactory.factory;


public class AnotherUIFactory implements AbstractFactory {

    @Override
    public AbstractUIItem createUIItem(String criteria) {
        return new PopupItem("Just another parent class");
    }

    @Override
    public AbstractUIContainer createUIContainer(String criteria) {
        return new FrameUIContainer();
    }

}

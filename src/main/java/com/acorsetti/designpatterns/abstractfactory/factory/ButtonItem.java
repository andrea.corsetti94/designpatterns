package com.acorsetti.designpatterns.abstractfactory.factory;

public class ButtonItem extends AbstractUIItem {

    private String specialName = "BIN";

    ButtonItem(String parentClass) {
        super(parentClass);
    }

    @Override
    String name() {
        return super.getParentClass() + " ButtonItemName: " + specialName;
    }

    public String getSpecialName() {
        return specialName;
    }

    public void setSpecialName(String specialName) {
        this.specialName = specialName;
    }
}

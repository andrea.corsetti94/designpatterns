package com.acorsetti.designpatterns.abstractfactory.factory;

public abstract class AbstractUIContainer {

    abstract int maxHeight();
    abstract int maxWidth();
}

package com.acorsetti.designpatterns.abstractfactory.factory;

public class BestUIFactory implements AbstractFactory {

    @Override
    public AbstractUIItem createUIItem(String criteria) {
        switch (criteria){
            case "Popup":
                return new PopupItem("Parent Class for Popup");
            case "Button":
                return new ButtonItem("Parent Class for Button");
            default:
                throw new UnsupportedOperationException("Unrecognized criteria for creating AbstractUIITem: " + criteria);
        }
    }

    @Override
    public AbstractUIContainer createUIContainer(String criteria) {
        switch (criteria){
            case "Widget":
                return new WidgetUIContainer();
            case "Frame":
                return new FrameUIContainer();
            default:
                throw new UnsupportedOperationException("Unrecognized criteria for creating AbstractUIContainer: " + criteria);
        }
    }

}

package com.acorsetti.designpatterns.abstractfactory.factory;

public abstract class AbstractUIItem {

    private String parentClass;

    abstract String name();

    AbstractUIItem(String parentClass){
        this.parentClass = parentClass;
    }

    protected String getParentClass(){
        return parentClass;
    }
}

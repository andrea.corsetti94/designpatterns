package com.acorsetti.designpatterns.abstractfactory.factory;

public class FrameUIContainer extends AbstractUIContainer {

    private int maxValue = 500;

    @Override
    int maxHeight() {
        return maxValue;
    }

    @Override
    int maxWidth() {
        return maxValue;
    }

    public int getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(int maxValue) {
        this.maxValue = maxValue;
    }
}

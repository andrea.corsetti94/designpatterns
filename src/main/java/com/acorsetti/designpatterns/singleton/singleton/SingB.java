package com.acorsetti.designpatterns.singleton.singleton;

import org.springframework.stereotype.Component;

/**
 * Automatically a Singleton because it's a component!
 */
@Component
public class SingB {
}

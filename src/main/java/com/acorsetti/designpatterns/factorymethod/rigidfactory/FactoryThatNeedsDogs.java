package com.acorsetti.designpatterns.factorymethod.rigidfactory;

import com.acorsetti.designpatterns.factorymethod.factory.Dog;
import com.acorsetti.designpatterns.factorymethod.factory.Pet;

public class FactoryThatNeedsDogs implements PetFactory {
    @Override
    public Pet createPet() {
        return new Dog();
    }
}

package com.acorsetti.designpatterns.factorymethod.rigidfactory;

import com.acorsetti.designpatterns.factorymethod.factory.Cat;
import com.acorsetti.designpatterns.factorymethod.factory.Pet;

public class FactoryThatNeedsCats implements PetFactory {
    @Override
    public Pet createPet() {
        return new Cat();
    }
}

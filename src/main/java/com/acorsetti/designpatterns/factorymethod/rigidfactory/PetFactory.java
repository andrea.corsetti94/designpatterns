package com.acorsetti.designpatterns.factorymethod.rigidfactory;

import com.acorsetti.designpatterns.factorymethod.factory.Pet;

/**
 * In this more rigid version of Factory Method, if you add one new type of Animal
 * you DONT'T have to change the behaviour of PetFactory interface like you should have
 * done with the "...factory.PetFactory".
*  The latter would have need modifications in order to add the new "case" in its switch
 * statement
 */
public interface PetFactory {

    Pet createPet();
}

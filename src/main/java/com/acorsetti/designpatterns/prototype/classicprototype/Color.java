package com.acorsetti.designpatterns.prototype.classicprototype;


/**
 * The prototype pattern has some benefits, for example:
 *
 * It eliminates the (potentially expensive) overhead of initializing an object;
 * It simplifies and can optimize the use case where multiple objects of the same type will have mostly the
 * same data;
 *
 * For example, say your program uses objects that are created from data parsed from motley unchanging
 * information retrieved over the network. Rather than retrieving the data and re-parsing it each time
 * a new object is created, the prototype pattern can be used to simply duplicate the original object
 * whenever a new one is needed.
 */
public abstract class Color implements Cloneable {

    public Color(){
        /*
        Many computational hard operations (i.e. read and construct object from DB)
         */
    }

    protected String colorName;

    abstract void addColor();

    public Object clone()
    {
        Object clone = null;
        try
        {
            clone = super.clone();
        }
        catch (CloneNotSupportedException e)
        {
            e.printStackTrace();
        }
        return clone;
    }

}

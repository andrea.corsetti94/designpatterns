package com.acorsetti.designpatterns.prototype.classicprototype;

public class Client {

    public static void main (String[] args)
    {
        ColorStore.getColor("blue").addColor();
        ColorStore.getColor("red").addColor();
        ColorStore.getColor("red").addColor();
        ColorStore.getColor("blue").addColor();
    }
}

package com.acorsetti.designpatterns.prototype.classicprototype;

public class BlueColor extends Color {

    public BlueColor(){
        super();
        /*
        Other computational hard operations.
         */
    }

    @Override
    void addColor() {
        System.out.println("Blue color added");
    }
}

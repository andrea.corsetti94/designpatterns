package com.acorsetti.designpatterns.prototype.classicprototype;

import java.util.HashMap;
import java.util.Map;

public class ColorStore {

    private static Map<String, Color> colorMap = new HashMap<String, Color>();

    static
    {
        colorMap.put("blue", new BlueColor());
        colorMap.put("red", new RedColor());
    }

    /*
    Returns a clone instead of the original
     */
    public static Color getColor(String colorName)
    {
        return (Color) colorMap.get(colorName).clone();
    }


}

package com.acorsetti.designpatterns.prototype.classicprototype;

public class RedColor extends Color {

    public RedColor(){
        super();
        /*
        Other computational hard operations.
         */
    }

    @Override
    void addColor() {
        System.out.println("Blue color added");
    }
}

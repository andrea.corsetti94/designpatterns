package com.acorsetti.designpatterns.prototype;

import com.acorsetti.designpatterns.prototype.springprototype.ProtoFalse;
import com.acorsetti.designpatterns.prototype.springprototype.ProtoTrue;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;

@SpringBootApplication
public class DesignPatternsAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(DesignPatternsAppApplication.class, args);
    }

    @Bean
    public ProtoFalse protoFalse(){
        return new ProtoFalse();
    }

    @Bean
    @Scope("springprototype")
    public ProtoTrue protoTrue(){
        return new ProtoTrue();
    }

}


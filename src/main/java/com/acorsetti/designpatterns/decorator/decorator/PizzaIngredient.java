package com.acorsetti.designpatterns.decorator.decorator;

public abstract class PizzaIngredient extends Pizza {
    public abstract String getDescription();
}

package com.acorsetti.designpatterns.decorator.classicDecorator;

public interface CommandProcessor {

    public Command receive(Request request);
    public Response execute(Command command);
    public void respond(Response response);


}

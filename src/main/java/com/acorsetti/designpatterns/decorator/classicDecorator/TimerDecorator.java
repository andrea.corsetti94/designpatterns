package com.acorsetti.designpatterns.decorator.classicDecorator;

/**
 * Timer decorator has to extend the command processor functionality, without changing the command processors
 * classes.
 * So it implements a command Processor but it's also COMPOSED BY a command processor (injected via constructor)
 * This allows for other decorators to build up the hierarchy and add even more functionalities.
 */
public class TimerDecorator implements CommandProcessor {

    //NOTABLE
    private final CommandProcessor commandProcessor;

    private final Timer timer;

    public TimerDecorator(CommandProcessor commandProcessor, Timer timer) {
        this.commandProcessor = commandProcessor;
        this.timer = timer;
    }

    @Override
    public Command receive(Request request) {
        this.timer.start();
        return this.commandProcessor.receive(request);
    }

    @Override
    public Response execute(Command command) {
        return this.commandProcessor.execute(command);
    }

    @Override
    public void respond(Response response) {
        this.commandProcessor.respond(response);
        this.timer.stop();
    }
}

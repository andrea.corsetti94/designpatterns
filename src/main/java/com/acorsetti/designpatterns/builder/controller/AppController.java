package com.acorsetti.designpatterns.builder.controller;

import com.acorsetti.designpatterns.builder.builder.Contact;
import com.acorsetti.designpatterns.builder.builder.ContactBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/")
public class AppController {


    @GetMapping
    public String getDefault(){
        return "{\"message\": \"Hello World\"}";
    }

    @GetMapping("presidents")
    public List<Contact> getPresidents(){
        List<Contact> contacts = new ArrayList<>();

        Contact contact = new Contact();
        contact.setFirstName("George");
        contact.setLastName("Washington");
        contacts.add(contact);

        contacts.add(new Contact("John", "Adams", null));

        contacts.add(
                new ContactBuilder()
                        .setFirstName("Thomas")
                        .setLastName("Jefferson")
                        .buildContact());
        
        return contacts;
    }

}

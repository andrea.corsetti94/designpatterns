package com.acorsetti.designpatterns.template;

/**
 * Frameworks work this way, they define the structure of the algorithm and let the client, who
 * provides the concrete implementations, define the single steps out of control of the framework.
 *
 * Hollywood principle: dont call us, we'll call you.
 *
 * The template pattern is used when a particular operation has some invariant behavior(s)
 * that can be defined in terms of other varying primitive behaviors.
 * The abstract class defines the invariant behavior(s), while the implementing classes defined
 * the dependent methods.
 */
public abstract class Worker {

    abstract void work();

    public void getUp(){
        System.out.println("Getting up");
    }

    public void eat(){
        System.out.println("Eating");
    }

    public void sleep(){
        System.out.println("sleeping");
    }

    /**
     * Algorithm.
     * Concrete implementations will implement "work" method but the algorithm stays the same
     */
    public void routine() {
        this.getUp(); //invariant
        this.eat(); //invariant
        this.work(); //variant
        this.eat(); //invariant
        this.sleep(); //invariant
    }
}

package com.acorsetti.designpatterns.template;

public class Employee extends Worker {
    @Override
    void work() {
        System.out.println("working as employee");
    }
}

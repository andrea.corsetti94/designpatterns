package com.acorsetti.designpatterns.template;

public class FootballPlayer extends Worker {
    @Override
    void work() {
        System.out.println("working as football player");
    }
}

package com.acorsetti.designpatterns.proxy;

public class ProxyImage implements Image {

    private RealImage realImage;
    private String filename;

    public ProxyImage(String filename) {
        this.filename = filename;
    }

    @Override
    public void doAction() {
        if ( this.realImage == null ){
            //gets called only once!
            this.realImage = new RealImage(this.filename);
        }

        //note that this mechanism allows to add functionality to the doAction method (like AOP...)
        this.realImage.doAction();
    }
}

package com.acorsetti.designpatterns.proxy;

public class RealImage implements Image {

    private String filename;

    public RealImage(String filename) {
        this.filename = filename;
        /*
            Do some very expensive work, like loading from hard disk
        */
    }

    @Override
    public void doAction() {
        System.out.println("Doing something");
    }
}

package com.acorsetti.designpatterns.proxy;

public interface Image {

    void doAction();
}

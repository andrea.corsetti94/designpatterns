package com.acorsetti.designpatterns.mvc;

import com.acorsetti.designpatterns.builder.builder.Contact;
import com.acorsetti.designpatterns.builder.builder.ContactBuilder;
import com.acorsetti.designpatterns.repository.PresidentEntity;
import com.acorsetti.designpatterns.repository.PresidentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/")
public class AppController {

    @GetMapping
    public String getDefault(){
        return "{\"message\": \"Hello World\"}";
    }


    @GetMapping("presidents")
    public List<Contact> getPresidents(){
        List<Contact> contacts = new ArrayList<>();

        Contact contact = new Contact();
        contact.setFirstName("George");
        contact.setLastName("Washington");
        contacts.add(contact);

        contacts.add(new Contact("John", "Adams", null));

        contacts.add(new ContactBuilder().setFirstName("Thomas").setLastName("Jefferson").buildContact());

        return contacts;
    }

    @Autowired
    PresidentRepository presidentRepository;

    @GetMapping("presidents/{id}")
    public PresidentEntity getPresById(@PathVariable Long id){
        return this.presidentRepository.findById(id).get();
    }

    @Autowired
    RestTemplate restTemplate;

    @GetMapping("presidentContact/{id}")
    public Contact getPresContById(@PathVariable Long id){
        PresidentEntity entity = this.restTemplate
                .getForEntity("http://localhost:8080/presidents/{id}",
                        PresidentEntity.class,
                        id).getBody();
        return (new ContactBuilder()
            .setFirstName(entity.getFirstName())
        .setLastName(entity.getLastName())
        .setEmailAddress(entity.getEmailAddress())
        .buildContact());
    }

}

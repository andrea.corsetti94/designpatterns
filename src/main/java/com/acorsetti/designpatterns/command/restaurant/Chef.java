package com.acorsetti.designpatterns.command.restaurant;

public class Chef implements PlateReceiver {


    @Override
    public void prepareDish(Dish dish) {
        System.out.println("Preparing dish: " + dish.toString());
    }
}

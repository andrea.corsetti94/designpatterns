package com.acorsetti.designpatterns.command.restaurant;

public interface PlateReceiver {

    void prepareDish(Dish dish);
}

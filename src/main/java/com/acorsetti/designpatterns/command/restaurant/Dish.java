package com.acorsetti.designpatterns.command.restaurant;

public class Dish {

    private String name;

    public Dish(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Dish{" +
                "name='" + name + '\'' +
                '}';
    }
}

package com.acorsetti.designpatterns.command.restaurant;

import com.acorsetti.designpatterns.command.Command;

public class PrepareDishCommand implements Command {

    private PlateReceiver plateReceiver;
    private Dish dish;

    public PrepareDishCommand(PlateReceiver plateReceiver, Dish dish) {
        this.plateReceiver = plateReceiver;
        this.dish = dish;
    }

    @Override
    public void execute() {
        this.plateReceiver.prepareDish(dish);
    }

}

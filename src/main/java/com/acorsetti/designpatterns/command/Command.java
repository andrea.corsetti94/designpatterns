package com.acorsetti.designpatterns.command;

/**
 * Command implementations will be provided with SwitchReceiver implementations, so when "execute" is called, the receiver
 * objects will perform their dirty tasks
 *
 */
public interface Command {

    void execute();
}

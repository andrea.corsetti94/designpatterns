package com.acorsetti.designpatterns.command;

import com.acorsetti.designpatterns.command.devices.*;
import com.acorsetti.designpatterns.command.restaurant.Chef;
import com.acorsetti.designpatterns.command.restaurant.Dish;
import com.acorsetti.designpatterns.command.restaurant.PrepareDishCommand;

public class DesignPatternsAppApplication {

    public static void main(String[] args){
        SwitchReceiver tvSwitchReceiver = new TV();
        SwitchReceiver radioSwitchReceiver = new Radio();

        Command switchOnTv = new SwitchOnCommand(tvSwitchReceiver);
        Command switchOnRadio = new SwitchOnCommand(radioSwitchReceiver);

        Command switchOffTv = new SwitchOffCommand(tvSwitchReceiver);
        Command switchOffRadio = new SwitchOffCommand(radioSwitchReceiver);

        Invoker invoker = new Invoker(switchOnTv);
        invoker.executeCommand();

        invoker = new Invoker(switchOffTv);
        invoker.executeCommand();

        invoker = new Invoker(switchOnRadio);
        invoker.executeCommand();

        invoker = new Invoker(switchOffRadio);
        invoker.executeCommand();

        //---------------------------

        Dish carbonara = new Dish("Carbonara");
        Dish cacioEpepe = new Dish("cacio e pepe");
        Chef canavacciuolo = new Chef(); // receiver

        Invoker cameriere = new Invoker(new PrepareDishCommand(canavacciuolo, carbonara));
        cameriere.executeCommand();

        cameriere = new Invoker(new PrepareDishCommand(canavacciuolo, cacioEpepe));
        cameriere.executeCommand();
    }


}


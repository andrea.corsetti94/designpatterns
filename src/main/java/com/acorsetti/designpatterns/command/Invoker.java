package com.acorsetti.designpatterns.command;

/**
 * Invoker doesnt know anything about the command is implemented! Thats the decoupling that the Command pattern provides.
 * Invoker is responsible for "starting" the command chain.
 */
public class Invoker {

    private Command command;

    public Invoker(Command command) {
        this.command = command;
    }

    public void executeCommand(){
        this.command.execute();
    }
}

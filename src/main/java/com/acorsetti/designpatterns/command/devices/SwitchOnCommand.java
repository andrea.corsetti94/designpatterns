package com.acorsetti.designpatterns.command.devices;

import com.acorsetti.designpatterns.command.Command;

public class SwitchOnCommand implements Command {

    private SwitchReceiver switchReceiver;

    public SwitchOnCommand(SwitchReceiver switchReceiver) {
        this.switchReceiver = switchReceiver;
    }

    @Override
    public void execute() {
        this.switchReceiver.switchOn();
    }


}

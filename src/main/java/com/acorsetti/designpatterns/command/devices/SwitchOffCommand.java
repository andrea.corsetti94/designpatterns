package com.acorsetti.designpatterns.command.devices;

import com.acorsetti.designpatterns.command.Command;

public class SwitchOffCommand implements Command {

    private SwitchReceiver switchReceiver;

    public SwitchOffCommand(SwitchReceiver switchReceiver) {
        this.switchReceiver = switchReceiver;
    }

    @Override
    public void execute() {
        this.switchReceiver.switchOff();
    }


}

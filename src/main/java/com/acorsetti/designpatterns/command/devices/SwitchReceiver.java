package com.acorsetti.designpatterns.command.devices;

/**
 * SwitchReceiver are the objects that do the dirty jobs, without exposing this dirty job to the callers
 */
public interface SwitchReceiver {

    void switchOn();
    void switchOff();
}

package com.acorsetti.designpatterns.command.devices;

public class TV implements SwitchReceiver {

    @Override
    public void switchOn() {
        /*
        Dirty details of how to switch on this device
         */

        System.out.println("Switching on TV");
    }

    @Override
    public void switchOff() {
        /*
        Dirty details of how to switch off this device
         */
        System.out.println("Switching off TV");
    }
}

package com.acorsetti.designpatterns.mediator;

public class Client extends ChatUser {

    private String username;

    public Client(Mediator mediator, String username) {
        super(mediator);
        this.username = username;
    }

    @Override
    public String toString() {
        return "Client{" +
                "username='" + username + '\'' +
                '}';
    }
}

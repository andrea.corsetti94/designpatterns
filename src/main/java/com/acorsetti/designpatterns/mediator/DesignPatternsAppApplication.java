package com.acorsetti.designpatterns.mediator;


public class DesignPatternsAppApplication {

    public static void main(String[] args){
        Mediator mediator = new Mediator();

        ChatUser client = new Client(mediator, "client 1");
        ChatUser callCenterOperator = new CallCenterOperator(mediator, "ABCD1");

        client.sendMessage(callCenterOperator, "Ciao");
        callCenterOperator.sendMessage(client, "Dica");
        client.sendMessage(callCenterOperator, "Sei un operatore incompetente");
        callCenterOperator.sendMessage(client, "incompetente ce sarai");

    }

}


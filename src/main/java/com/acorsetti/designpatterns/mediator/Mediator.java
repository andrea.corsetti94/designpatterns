package com.acorsetti.designpatterns.mediator;

public class Mediator {

    public void sendMessage(ChatUser destination, String message){
        System.out.println("message : " + message + " sent to: " + destination.toString());
    }
}

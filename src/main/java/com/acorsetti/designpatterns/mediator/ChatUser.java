package com.acorsetti.designpatterns.mediator;

public class ChatUser {

    private Mediator mediator;

    public ChatUser(Mediator mediator) {
        this.mediator = mediator;
    }

    public void sendMessage(ChatUser chatUser, String message){
        this.mediator.sendMessage(chatUser, message);
    }
}

package com.acorsetti.designpatterns.mediator;

public class CallCenterOperator extends ChatUser{

    private String id;

    public CallCenterOperator(Mediator mediator, String id) {
        super(mediator);
        this.id = id;
    }

    @Override
    public String toString() {
        return "CallCenterOperator{" +
                "id='" + id + '\'' +
                '}';
    }


}

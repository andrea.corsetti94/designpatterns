package com.acorsetti.designpatterns.adapter.frankmadapter;

public interface Orange {
    String getVariety();
    void eat();
    void peel();
    void juice();
}

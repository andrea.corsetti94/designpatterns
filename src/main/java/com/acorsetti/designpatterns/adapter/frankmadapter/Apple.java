package com.acorsetti.designpatterns.adapter.frankmadapter;

public interface Apple {
    String getVariety();
    void eat();
}

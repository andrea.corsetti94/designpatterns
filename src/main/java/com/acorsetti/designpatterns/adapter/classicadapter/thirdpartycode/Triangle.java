package com.acorsetti.designpatterns.adapter.classicadapter.thirdpartycode;

public class Triangle implements GeometricShape {

    private final double a;
    private final double b;
    private final double c;

    public Triangle(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public Triangle() {
        this(1,1,1);
    }

    @Override
    public double area() {
        double s = (a + b + c) / 2;
        return Math.sqrt(s * (s - a) * (s - b) * (s - c));
    }

    @Override
    public double perimeter() {
        return a + b + c;
    }

    @Override
    public void drawShape() {
        System.out.println("Drawing Triangle with area: " + area() + " and perimeter: " + perimeter());
    }
}

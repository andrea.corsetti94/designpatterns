package com.acorsetti.designpatterns.adapter.classicadapter;

import com.acorsetti.designpatterns.adapter.classicadapter.thirdpartycode.Rhombus;
import com.acorsetti.designpatterns.adapter.classicadapter.thirdpartycode.Triangle;

import java.util.ArrayList;
import java.util.List;

public class DrawingClient {

    List<Shape> shapes = new ArrayList<>();
    public DrawingClient() {
        super();
    }
    public void addShape(Shape shape) {
        shapes.add(shape);
    }
    public List<Shape> getShapes() {
        return new ArrayList<>(shapes);
    }
    public void draw() {
        if (shapes.isEmpty()) {
            System.out.println("Nothing to draw!");
        } else {
            shapes.stream().forEach(shape -> shape.draw());
        }
    }
    public void resize() {
        if (shapes.isEmpty()) {
            System.out.println("Nothing to resize!");
        } else {
            shapes.stream().forEach(shape -> shape.resize());
        }
    }


    public static void main(String[] args) {

        //BEFORE USING THIRDPARTY CODE INTERFACES
        System.out.println("Creating drawing of shapes...");
        DrawingClient drawing = new DrawingClient();
        drawing.addShape(new Rectangle());
        drawing.addShape(new Circle());
        System.out.println("Drawing...");
        drawing.draw();
        System.out.println("Resizing...");
        drawing.resize();



        //AFTER USING THIRDPARTY CODE AND ADAPTING IT:

        System.out.println("Creating drawing of shapes...");
        DrawingClient anotherDrawing = new DrawingClient();
        anotherDrawing.addShape(new Rectangle());
        anotherDrawing.addShape(new Circle());
        anotherDrawing.addShape(new GeometricShapeObjectAdapter(new Triangle()));
        anotherDrawing.addShape(new GeometricShapeObjectAdapter(new Rhombus()));
        System.out.println("Drawing...");
        anotherDrawing.draw();
        System.out.println("Resizing...");
        anotherDrawing.resize();
    }
}

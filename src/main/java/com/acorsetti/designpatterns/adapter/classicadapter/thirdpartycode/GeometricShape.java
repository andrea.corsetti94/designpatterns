package com.acorsetti.designpatterns.adapter.classicadapter.thirdpartycode;

public interface GeometricShape {

    double area();
    double perimeter();
    void drawShape();

}

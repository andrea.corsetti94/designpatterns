package com.acorsetti.designpatterns.adapter.classicadapter;

import com.acorsetti.designpatterns.adapter.classicadapter.thirdpartycode.GeometricShape;
import com.acorsetti.designpatterns.adapter.classicadapter.thirdpartycode.Rhombus;
import com.acorsetti.designpatterns.adapter.classicadapter.thirdpartycode.Triangle;

public class GeometricShapeObjectAdapter implements Shape {
    // implements "our" interface

    private GeometricShape adaptee;
    public GeometricShapeObjectAdapter(GeometricShape adaptee) {
        super();
        this.adaptee = adaptee;
    }

    /**
     * Adapts "our" draw method
     */
    @Override
    public void draw() {
        adaptee.drawShape();
    }

    /**
     * Adapts "our" resize method
     */
    @Override
    public void resize() {
        System.out.println(description() + " can't be resized. Please create new one with required values.");
        throw new UnsupportedOperationException();
    }

    /**
     * Adapts "our" description method
     * @return
     */
    @Override
    public String description() {
        if (adaptee instanceof Triangle) {
            return "Triangle object";
        } else if (adaptee instanceof Rhombus) {
            return "Rhombus object";
        } else {
            return "Unknown object";
        }
    }
    @Override
    public boolean isHide() {
        return false;
    }
}

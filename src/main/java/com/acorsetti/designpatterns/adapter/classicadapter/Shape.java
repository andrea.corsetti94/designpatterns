package com.acorsetti.designpatterns.adapter.classicadapter;

public interface Shape {

    void draw();
    void resize();
    String description();
    boolean isHide();

}

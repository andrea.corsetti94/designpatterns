package com.acorsetti.designpatterns.observer;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class Rete4Channel implements PropertyChangeListener {

    Rete4Channel(NewsAgency newsAgency) {
        newsAgency.addListener(this);
    }

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        System.out.println("Rete4 here. Event: " + propertyChangeEvent.getPropertyName() + " detail: "
                + propertyChangeEvent.getNewValue().toString());
    }
}

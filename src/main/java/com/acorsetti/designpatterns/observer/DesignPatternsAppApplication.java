package com.acorsetti.designpatterns.observer;


import java.time.LocalDateTime;

public class DesignPatternsAppApplication {

    public static void main(String[] args) {

        NewsAgency newsAgency = new NewsAgency();

        BBCChannel bbcChannel = new BBCChannel(newsAgency);
        Radio101Channel radio101Channel = new Radio101Channel(newsAgency);
        Rete4Channel rete4Channel = new Rete4Channel(newsAgency);



        newsAgency.addNews(new News("ID1", "news1", LocalDateTime.now()));

        newsAgency.addNews(new News("ID2", "news2", LocalDateTime.now()));

        newsAgency.addNews(new News("ID3", "news3", LocalDateTime.now()));

        newsAgency.removeListener(rete4Channel);

        newsAgency.addNews(new News("ID4", "news4", LocalDateTime.now()));


    }

}


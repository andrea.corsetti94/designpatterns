package com.acorsetti.designpatterns.observer;


import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.List;


/**
 * The news agency is the "observable" or the subject. It changes its state everytime  a news gets added to its list.
 */
public class NewsAgency  {

    private List<News> newsList = new ArrayList<>();


    PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    public void addNews(News news){
        newsList.add(news);

        //update method
        changeSupport.firePropertyChange("news is added", null, news);
    }

    public List<News> getNews(){
        return newsList;
    }


    /**
     * This method adds a listener that is notified everytime a property change is fired.
     * (like "addObserver")
     * @param listener
     */
    public void addListener(PropertyChangeListener listener){
        this.changeSupport.addPropertyChangeListener(listener);
    }

    /**
     * This method removes a listener from listening to property changes.
     * (like "removeObserver")
     *
     * @param listener
     */
    public void removeListener(PropertyChangeListener listener){
        this.changeSupport.removePropertyChangeListener(listener);
    }

}

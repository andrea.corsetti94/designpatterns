package com.acorsetti.designpatterns.observer;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class BBCChannel implements PropertyChangeListener {

    BBCChannel(NewsAgency newsAgency){
        newsAgency.addListener(this);
    }

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        System.out.println("BBC here. Event: " +propertyChangeEvent.getPropertyName() +
                " value: " + propertyChangeEvent.getNewValue().toString());
    }

}

package com.acorsetti.designpatterns.observer;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class Radio101Channel implements PropertyChangeListener {

    Radio101Channel(NewsAgency newsAgency) {
        newsAgency.addListener(this);
    }

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        System.out.println("Radio101 here: " + propertyChangeEvent.getPropertyName() + " Detail: " +
                propertyChangeEvent.getNewValue().toString());
    }
}
